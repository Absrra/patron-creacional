import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonComponent } from './components/person/person.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'persona',
    pathMatch: 'full'
  },
  {
    path: 'persona',
    component: PersonComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
