import { Component, OnInit } from '@angular/core';
import { CreatorpersonaFactory } from '../../factory/creatorpersona.factory';
import { IPersona } from '../../models/persona.model';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss'],
  providers: [
    CreatorpersonaFactory
  ]
})
export class PersonComponent implements OnInit {

  public dataPersona: IPersona;

  constructor(
    private Creatorpersona: CreatorpersonaFactory
  ) { }

  ngOnInit() {
    this.dataPersona  = this.Creatorpersona.createPerson('Estudiante');
    console.log(this.dataPersona.getFirst_name());
  }

}
